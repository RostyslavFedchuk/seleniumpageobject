package com.epam.model;

import java.time.LocalTime;

public class Email {
    private String receiver;
    private String subject;
    private String message;
    private LocalTime time;

    private Email() {
    }

    public String getReceiver() {
        return receiver;
    }

    public String getSubject() {
        return subject;
    }

    public String getMessage() {
        return message;
    }

    public LocalTime getTime() {
        return time;
    }

    public static Builder newBuilder() {
        return new Email().new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public Builder setMessage(String message) {
            Email.this.message = message;
            return this;
        }

        public Builder setReceiver(String receiver) {
            Email.this.receiver = receiver;
            return this;
        }

        public Builder setSubject(String subject) {
            Email.this.subject = subject;
            return this;
        }

        public Email build() {
            Email.this.time = LocalTime.now();
            return Email.this;
        }
    }

    @Override
    public String toString() {
        return "Email{" +
                "receiver='" + receiver + '\'' +
                ", subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                ", time=" + time +
                '}';
    }
}
