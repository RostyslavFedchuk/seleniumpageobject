package com.epam.po;

import com.epam.elements.implementations.Button;
import com.epam.elements.implementations.TextInput;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GmailPage extends BasePage {
    @FindBy(css = "div.aic div[role='button']")
    private Button composeButton;
    @FindBy(css = "textarea[name='to']")
    private TextInput receiverField;
    @FindBy(css = "input[name='subjectbox']")
    private TextInput subjectField;
    @FindBy(xpath = "//div[@class='Ar Au']/*[@role='textbox']")
    private TextInput messageField;
    @FindBy(xpath = "//div[@class='dC']/*[@role='button']")
    private Button sendButton;
    @FindBy(id = "link_vsm")
    private Button showMessageButton;
    @FindBy(xpath = "//div[@gh='mtb']//div[1]//div[2]//div[3]")
    private Button deleteMessageButton;

    @Step("Clicking Compose button step...")
    public void clickComposeButton() {
        composeButton.click();
    }

    @Step("Filling Receiver field with receiver: {0} step...")
    public void fillReceiverField(String receiver) {
        receiverField.sendKeys(receiver);
    }

    @Step("Filling Subject field with subject: {0} step...")
    public void fillSubjectField(String subject) {
        subjectField.sendKeys(subject);
    }

    @Step("Filling Message field with message: {0} step...")
    public void fillMessageField(String message) {
        messageField.sendKeys(message);
    }

    @Step("Clicking Send button step...")
    public void clickSendButton() {
        sendButton.click();
    }

    @Step("Clicking ShowMassage button step...")
    public void clickShowMessageButton() {
        showMessageButton.click();
    }

    @Step("Clicking DeleteMessage button step...")
    public void clickDeleteMessage() {
        deleteMessageButton.click();
    }

    @Step("Trying to find UndoDeletingButton step...")
    public List<WebElement> getUndoDeletingButton() {
        return driver.findElements(By.id("link_undo"));
    }
}
