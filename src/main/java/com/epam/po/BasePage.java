package com.epam.po;

import com.epam.elements.CustomDecorator;
import com.epam.utils.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public class BasePage {
    WebDriver driver;

    BasePage() {
        init();
    }

    private void init() {
        driver = WebDriverManager.getDriver();
        PageFactory.initElements(new CustomDecorator(new DefaultElementLocatorFactory(driver)), this);
    }
}
