package com.epam.po;

import com.epam.elements.implementations.Button;
import com.epam.elements.implementations.TextInput;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

public class AuthorizationPage extends BasePage {
    @FindBy(css = "input#identifierId[type='email']")
    private TextInput emailInput;
    @FindBy(xpath = "//*[@id='password']//div[1]//div//div[1]//input")
    private TextInput passwordInput;
    @FindBy(css = "span.RveJvd.snByac")
    private Button submitEmailButton;
    @FindBy(css = "div#passwordNext[role='button']")
    private Button submitPasswordButton;

    @Step("Navigating to gmail page step...")
    public void navigateToGmail() {
        driver.get("https://mail.google.com/mail");
    }

    @Step("Filling Email field with email: {0} step...")
    public void fillEmail(String email) {
        emailInput.sendKeys(email);
    }

    @Step("Clicking SubmitEmail button step...")
    public void submitEmail() {
        submitEmailButton.click();
    }

    @Step("Filling Password field with password: {0} step...")
    public void fillPassword(String password) {
        passwordInput.sendKeys(password);
    }

    @Step("Clicking SubmitPassword button step...")
    public void submitPassword() {
        submitPasswordButton.click();
    }
}
