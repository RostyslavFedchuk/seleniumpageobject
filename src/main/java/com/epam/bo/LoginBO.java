package com.epam.bo;

import com.epam.model.User;
import com.epam.po.AuthorizationPage;

public class LoginBO {
    private AuthorizationPage authorizationPage;

    public LoginBO() {
        authorizationPage = new AuthorizationPage();
    }

    public void login(User user) {
        authorizationPage.navigateToGmail();
        authorizationPage.fillEmail(user.getEmail());
        authorizationPage.submitEmail();
        authorizationPage.fillPassword(user.getPassword());
        authorizationPage.submitPassword();
    }
}
