package com.epam.bo;

import com.epam.model.Email;
import com.epam.po.GmailPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;

import java.util.List;

public class GmailBO {
    private GmailPage gmailPage;

    public GmailBO() {
        gmailPage = new GmailPage();
    }

    public void sendMessage(Email email) {
        gmailPage.clickComposeButton();
        gmailPage.fillReceiverField(email.getReceiver());
        gmailPage.fillSubjectField(email.getSubject());
        gmailPage.fillMessageField(email.getMessage());
        gmailPage.clickSendButton();
    }

    public void deleteJustWrittenMessage() {
        gmailPage.clickShowMessageButton();
        gmailPage.clickDeleteMessage();
    }

    public boolean isUndoDeletingButtonPresent() {
        return !gmailPage.getUndoDeletingButton().isEmpty();
    }
}
