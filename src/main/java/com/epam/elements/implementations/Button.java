package com.epam.elements.implementations;

import com.epam.elements.BaseElement;
import com.epam.utils.WebWait;
import org.openqa.selenium.WebElement;

public class Button extends BaseElement {

    public Button(WebElement element) {
        super(element);
    }

    public void click() {
        WebWait.waitForElementToBeClickable(element).click();
    }
}
