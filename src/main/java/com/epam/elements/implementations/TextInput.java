package com.epam.elements.implementations;

import com.epam.elements.BaseElement;
import com.epam.utils.WebWait;
import org.openqa.selenium.WebElement;

public class TextInput extends BaseElement {

    public TextInput(WebElement element) {
        super(element);
    }

    public void sendKeys(String text) {
        WebWait.waitForElementToBeVisible(element);
        clear();
        super.sendKeys(text);
    }
}
