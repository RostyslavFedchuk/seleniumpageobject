package com.epam.utils;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ResourceBundle;

public class WebWait {
    private static final int EXPLICIT_TIME_OUT;

    static {
        EXPLICIT_TIME_OUT = Integer.valueOf(ResourceBundle.getBundle("config")
                .getString("EXPLICIT_TIME_OUT"));
    }

    public static WebElement waitForElementToBeVisible(WebElement element) {
        return (new WebDriverWait(WebDriverManager.getDriver(), EXPLICIT_TIME_OUT))
                .until(ExpectedConditions.visibilityOf(element));
    }

    public static WebElement waitForElementToBeClickable(WebElement element) {
        return (new WebDriverWait(WebDriverManager.getDriver(), EXPLICIT_TIME_OUT))
                .until(ExpectedConditions.elementToBeClickable(element));
    }
}
