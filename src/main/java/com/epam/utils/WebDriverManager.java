package com.epam.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.isNull;

public class WebDriverManager {
    private static ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    static {
        ResourceBundle bundle = ResourceBundle.getBundle("config");
        System.setProperty(bundle.getString("WEB_DRIVER_NAME")
                , bundle.getString("PATH_TO_DRIVER"));
    }

    public static WebDriver getDriver() {
        if (isNull(driver.get())) {
            initDriver();
        }
        return driver.get();
    }

    private static void initDriver() {
        driver.set(new ChromeDriver());
        driver.get().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get().manage().window().maximize();
    }

    public static void quit() {
        driver.get().quit();
        driver.set(null);
    }
}
