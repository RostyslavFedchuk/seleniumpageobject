package com.epam.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {
    public static String[][] readFromCSV(String csvPath, String delimiter) {
        List<String[]> data = new ArrayList<>();
        String line = "";
        try (BufferedReader br = new BufferedReader(new FileReader(csvPath))) {
            while ((line = br.readLine()) != null) {
                data.add(line.split(delimiter));
            }
        } catch (IOException e) {
            System.out.println("Could not read from CSV file!");
            e.printStackTrace();
        }
        return data.toArray(new String[data.size()][]);
    }
}
