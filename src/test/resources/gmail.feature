Feature: Send and delete an email in Gmail

  Scenario Outline: Send and delete just written email
    When I fill the "<login>" and the "<password>"
    And I send defined email
    And I delete just written email
    Then The undo deleting button should be present

    Examples:
      | login                        | password       |
      | zzhak.EvKalipt@gmail.com     | love_T0Rtuk    |
      | Stepthan.fedoriv@gmail.com   | geri)_w31L     |
      | lvivskijgeorgij@gmail.com    | passW0rD_1     |
      | romankar323@gmail.com        | passW0rD_1     |
      | nazar.fedchuk13@gmail.com    | gtr13@.fmf2    |