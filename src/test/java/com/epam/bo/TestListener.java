package com.epam.bo;

import com.epam.utils.WebDriverManager;
import io.qameta.allure.Attachment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener {
    private static final Logger LOGGER = LogManager.getLogger(ITestListener.class);

    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenShotsPNG(WebDriver driver){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }

    @Attachment(value = "{0}", type = "text/plain")
    public static String saveTextLog(String message){
        return message;
    }

    private static String getTestMethodName(ITestResult result){
        return result.getMethod().getConstructorOrMethod().getName();
    }

    @Override
    public void onTestFailure(ITestResult result){
        LOGGER.info("Test Failure!!! Screenshot captured for test case: "
                + getTestMethodName(result));
        saveScreenShotsPNG(WebDriverManager.getDriver());
        saveTextLog(getTestMethodName(result) + " and screenshot taken!");
    }
}
