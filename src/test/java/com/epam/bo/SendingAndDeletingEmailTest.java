package com.epam.bo;

import com.epam.model.Email;
import com.epam.model.User;
import com.epam.utils.CSVReader;
import com.epam.utils.WebDriverManager;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.ResourceBundle;

import static org.testng.Assert.assertTrue;

@Listeners({TestListener.class})
public class SendingAndDeletingEmailTest {
    private static ResourceBundle bundle = ResourceBundle.getBundle("config");

    private Email getEmail() {
        return Email.newBuilder()
                .setMessage("Hello my dear friend!")
                .setReceiver(bundle.getString("EMAIL_RECEIVER"))
                .setSubject("SeleniumTest")
                .build();
    }

    @DataProvider(name = "accounts", parallel = true)
    public Object[][] getAccounts() {
        return CSVReader.readFromCSV("src/main/resources/accounts.csv", ":");
    }

    @Test(dataProvider = "accounts")
    public void testSendAndDeleteEmail(String login, String password) {
        LoginBO loginBO = new LoginBO();
        loginBO.login(new User(login, password));
        GmailBO gmail = new GmailBO();
        gmail.sendMessage(getEmail());
        gmail.deleteJustWrittenMessage();
        assertTrue(gmail.isUndoDeletingButtonPresent());
    }

    @AfterMethod
    public void quit() {
        WebDriverManager.quit();
    }
}