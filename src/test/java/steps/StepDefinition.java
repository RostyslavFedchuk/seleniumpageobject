package steps;

import com.epam.bo.GmailBO;
import com.epam.bo.LoginBO;
import com.epam.model.Email;
import com.epam.model.User;
import com.epam.utils.WebDriverManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.ResourceBundle;

import static org.testng.Assert.assertTrue;

public class StepDefinition {
    private static ResourceBundle bundle = ResourceBundle.getBundle("config");
    private LoginBO loginBO;
    private GmailBO gmail;

    public StepDefinition(){
        loginBO = new LoginBO();
        gmail = new GmailBO();
    }

    private Email getEmail() {
        return Email.newBuilder()
                .setMessage("Hello my dear friend!")
                .setReceiver(bundle.getString("EMAIL_RECEIVER"))
                .setSubject("SeleniumTest")
                .build();
    }

    @When("^I fill the \"([^\"]*)\" and the \"([^\"]*)\"$")
    public void iFillTheAndThe(String login, String password) {
        loginBO.login(new User(login, password));
    }

    @And("^I send defined email$")
    public void iSendDefinedEmail() {
        gmail.sendMessage(getEmail());
    }

    @And("^I delete just written email$")
    public void iDeleteJustWrittenEmail() {
        gmail.deleteJustWrittenMessage();
    }

    @Then("^The undo deleting button should be present$")
    public void theUndoDeletingButtonShouldBePresent() {
        assertTrue(gmail.isUndoDeletingButtonPresent());
        WebDriverManager.quit();
    }
}
